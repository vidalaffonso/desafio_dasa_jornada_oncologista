# aqui foi criado a classe HomePage usando a gem SitePrism para facilicar o
#mapeamento dos elementos

class HomePage < SitePrism::Page

  #aqui é setado a url padrão que está salva na pasta ambiente homolog.yml

  set_url ""

  element :cookies, "button[title='Fechar modal']"
  element :somos_dasa, "a[label='Somos Dasa'] .sc-1u8it46-0.d59fon-0.iUVrrJ"
  element :nossas_empresas, "#cG9zdDo0ODQ5"

  # aqui é criado o método para clicar na nossas empresas da home_page
  def filtros
    self.cookies.click
    self.somos_dasa.hover
    self.nossas_empresas.hover.click
  end
end
