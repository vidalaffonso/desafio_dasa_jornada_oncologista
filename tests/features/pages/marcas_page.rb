class MarcasPage < SitePrism::Page
  element :regioes, ".sc-1xia3sz-0.sc-1s5k36b-0.sc-1atgl38-0.gGWfCF.dDNRpm.gplCLt [label='São Paulo']"
  element :delboni, "img[src*='delboni.png']"
  # element :closechat, "span._hj-2RA7u__EmotionIconDefault__expressionIcon"

  def selecionar_filtros
    self.regioes.click
    scroll_to(page.find(".sc-1u8it46-0.hDPnqa", visible: false))
    self.delboni.click
  end
end
