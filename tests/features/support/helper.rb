#Faz Screenshot do nos
module Helper
  def tira_foto(file_name, resultado)
    temp_shot = page.save_screenshot("results/evidencias/#{$data}/temp_screen#{$h_m_s}.png")
    shot = Base64.encode64(File.open(temp_shot, "rb").read)
    attach(shot, "image/png")
  end

  def temp_shot
    temp_shot = page.save_screenshot("results/evidencias/#{$data}/temp_shot#{$h_m_s}.png")
    shot = Base64.encode64(File.open(temp_shot, "rb").read)
  end
end
