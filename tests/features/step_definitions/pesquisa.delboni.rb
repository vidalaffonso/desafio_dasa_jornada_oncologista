Dado("que esteja no site da Dasa") do
  home_page.load
end

Quando("é aplicado os filtros") do
  home_page.filtros
  marcas_page.selecionar_filtros
end

Então("sou direcionado para o site da Delboni Auriemo.") do
  # foi feita uma concatenação da mesangem  para não deixar chumbado a mensagem no step
  #que vem da ValidacaoPage
  #utilizando o  if e else se caso der OK ou NOK a mensagem é exibida no prompt!

  #if page.has_title? "Delboni Auriemo | Laboratório de Exames e de Imagem"
  page.has_title?(validacao_page.titulo_pagina)
end
